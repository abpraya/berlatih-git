<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <h1>Buat Account Baru!</h1>
  <h2>Sign Up Form</h2>

  <form action="/send" method="POST">
    @csrf
    <!-- Form nama -->
    <label>First Name:</label> <br> <br>
    <input type="text" name="namapertama"> <br> <br>
    <label>Last Name:</label> <br> <br>
    <input type="text" name="namaterakhir"> <br> <br>

    <!--radio  -->
    <label>Gender:</label><br><br>
    <input type="radio" name="gender"> Male <br>
    <input type="radio" name="gender"> Female <br>
    <input type="radio" name="gender"> Other <br><br>

    <!-- Option select  -->
    <label>Nasionality</label><br><br>
    <select name="nasionality" id="">
        <option value="">Indonesian</option>
        <option value="">Malaysian</option>
        <option value="">Australian</option>
    </select> <br><br>

    <!-- checkbox -->
    <label>Language Spoken:</label> <br><br>
    <input type="checkbox" name="" id=""> Bahasa Indonesia <br>
    <input type="checkbox" name="" id=""> English <br>
    <input type="checkbox" name="" id=""> Other <br><br>

    <label>Bio</label> <br><br>
    <textarea cols="30" rows="10"></textarea> <br><br>
    <!-- <input type="text" name="" id="" style="font-size: 8pt; height: 100px; width:280px;" > -->

    <input type="submit" value="kirim">
  </form>
</body>
</html>