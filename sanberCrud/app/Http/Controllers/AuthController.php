<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
   public function register(){
       return view('page.register');
   }

   public function send(Request $request){
    //    dd($request->all());
        $namapertama = $request["namapertama"];
        $namaterakhir = $request["namaterakhir"];
        $gender = $request["gender"];
        $nasionality = $request["nasionality"];

        return view('page.home', compact('namapertama', 'namaterakhir', 'gender', 'nasionality'));
   }
}
