<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class castController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast,nama|max:255',
            'umur' => 'required',
            'bio' => 'required'
        ]);


        dd($request->all());
    }
}
