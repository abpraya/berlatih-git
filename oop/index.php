<?php 

require('animal.php');
require('ape.php');
require('frog.php');


echo "BINATANG PERTAMA <br>" . "--------------------- <br>";
$binatang = new Animal('shaun');
echo "Name : " . " " . $binatang->name . "<br> ";
echo "Legs : " . " " . $binatang->legs . "<br>";
echo "Cold Blooded : " . " " . $binatang->cold_blooded . "<br>";

echo "---------------------<br>";
echo "<br>";
echo "<br>";

echo "BINATANG KEDUA <br>" . "--------------------- <br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . " " . $binatang->name . "<br> ";
echo "Legs : " . " " . $binatang->legs . "<br>";
echo "Cold Blooded : " . " " . $binatang->cold_blooded . "<br>";
echo "Yell :" . " " . $sungokong->yell("Auooo") . "<br>";  // "Auooo"

echo "---------------------<br>";
echo "<br>";
echo "<br>";

echo "BINATANG KETIGA <br>" . "--------------------- <br>";

$kodok = new Frog("kera sakti");
echo "Name : " . " " . $binatang->name . "<br> ";
echo "Legs : " . " " . $binatang->legs . "<br>";
echo "Cold Blooded : " . " " . $binatang->cold_blooded . "<br>";
echo "Yell :" . " " . $kodok->jump("Hop Hop") . "<br>";  // "Auooo"
echo "---------------------";



?>