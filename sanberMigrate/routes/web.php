<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/send', 'AuthController@send');
Route::get('/adminn', function() {
    return view('layouts.master');
});
Route::get('/table', function() {
    return view('layouts.table');
});
Route::get('/data-tables', function() {
    return view('layouts.dataTable');
} );
// Route::get('/', function() {
//     return view('welcome');
// });
